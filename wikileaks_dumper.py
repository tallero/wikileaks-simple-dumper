#!/usr/bin/env python3

# -*- coding: utf-8 -*-

#    Wikileaks dumper
#
#    ----------------------------------------------------------------------
#    Copyright © 2019  Pellegrino Prevete
#
#    All rights reserved
#    ----------------------------------------------------------------------
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

from requests import get
from requests.exceptions import ConnectionError
from bs4 import BeautifulSoup
from os import mkdir
from os.path import exists, join

def download(url):
    download_success = False
    while not download_success:
        try:
            page = get(url)
            download_success = True
        except ConnectionError as e:
            print("Error downloading", current_url)
    return page

base_url = 'https://file.wikileaks.org'

directories = ['file']

i = 0

while True:
    try:
        dir = directories[i]
        print("Reading", dir)
        current_url = join(base_url, dir)
        print("Create directory", dir)
        try:
            mkdir(dir)
        except FileExistsError as e:
            pass
        print("Opening", current_url)
        page = download(current_url)
        content = page.content
        soup = BeautifulSoup(content, 'html.parser')
        uris = soup.find_all('a')
        for uri in uris:
            path =  join(dir, uri['href'])
            complete_url = join(base_url, path)
            if path.endswith('/'):
                print("Found directory", complete_url)
                directories.append(path)
            elif not exists(path):
                print("Dowloading", complete_url)
                raw = download(complete_url).content
                with open(path, "wb") as f:
                    f.write(raw)
            i += 1
    except Exception as e:
        raise e

