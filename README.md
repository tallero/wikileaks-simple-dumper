## Simple Wikileaks dumper

It downloads recursively the files at https://file.wikileaks.org/file

Actually it can be used to download recursively any directory tree.

To use it, just run

    python3 wikileaks_dumper.py
